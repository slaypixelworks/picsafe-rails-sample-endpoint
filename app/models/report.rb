class Report < ActiveRecord::Base

  validates :uuid, presence: true, uniqueness: true
  validates :keyserver_url, presence: true
  validates :owner_id, presence: true

  mount_uploader :encrypted_file, EncryptedReportUploader

  def owner_name
    [ owner_first_name, owner_last_name ].reject(&:blank?).join(" ")
  end

end
