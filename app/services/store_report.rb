require 'zip'

class StoreReport

  class Error < StandardError; end

  def self.perform(params)
    token, picsafe_file = params[:token], params[:picsafe_file]

    token_json = decrypt_token(token)
    package_json, uuid, encrypted_file = unpack_picsafe_file(picsafe_file.tempfile, picsafe_file.original_filename)

    Report.create!(
      keyserver_url:     package_json["keyserver_url"],
      uuid:              uuid,
      owner_id:          token_json["id"],
      owner_email:       token_json["email"],
      owner_first_name:  token_json["first_name"],
      owner_last_name:   token_json["last_name"],
      owner_country:     token_json["country"],
      encrypted_file:    encrypted_file
    )
  ensure
    File.delete(encrypted_file.path) unless encrypted_file.nil?
  end

  private

  def self.decrypt_token(token)
    secret = Rails.application.secrets.key_server_secret
    res = JWT.decode token, secret, true, { algorithm: 'HS256' }
    res.first
  rescue JWT::DecodeError
    raise Error, "Invalid token"
  end

  def self.unpack_picsafe_file(picsafe_file, filename)
    raise Error, "No PicSafe file" unless picsafe_file

    package_json   = nil
    uuid           = filename.split(".").first
    encrypted_file = nil

    Zip::File.open(picsafe_file) do |zip|
      package_json   = parse_package_json(zip)
      encrypted_file = extract_encrypted_file(zip, uuid)
    end

    return package_json, uuid, encrypted_file
  end

  def self.parse_package_json(zip)
    entry = zip.glob('package.json').first
    raise Error, "Invalid PicSafe file (no package.json)" unless entry

    JSON.parse(entry.get_input_stream.read)
  end

  def self.extract_encrypted_file(zip, uuid)
    tmp_path = File.join(Dir.tmpdir, "#{uuid}.encrypted")
    File.delete(tmp_path) if File.exists?(tmp_path)

    entry = zip.glob('package.encrypted').first
    raise Error, "Invalid PicSafe file (no package.encrypted)" unless entry

    entry.extract(tmp_path)
    File.new(tmp_path)
  end

end
