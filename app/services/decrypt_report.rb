class DecryptReport

  class Error < StandardError; end
  class DecryptionError < Error; end

  def self.perform(report)
    decrypted_file = Tempfile.create([ report.uuid, ".zip" ])
    decrypted_file.binmode

    secret = get_secret(report)
    encrypted_data = read_encrypted_data(report)
    decrypt(encrypted_data, secret, decrypted_file)

    decrypted_file
  rescue DecryptionError => e
    Rails.logger.error "Unable to decrypt the report #{report.id}:\n#{e.message}"
    raise Error, "Unable to decrypt the report"
  end

  private

  def self.get_secret(report)
    token = Rails.application.secrets.key_server_token
    query = { report_id: report.uuid, token: token }.to_query
    uri = URI.parse("#{report.keyserver_url}/decryption_key?#{query}")
    json = uri.read

    data = JSON.parse(json)
    if data["success"]
      data["decryption_key"]
    else
      raise Error, "Unable to get decryption key: #{data["error"]}"
    end
  rescue Timeout::Error
    raise Error, "Unable to reach key server"
  end

  def self.read_encrypted_data(report)
    File.open(report.encrypted_file.path, "rb").read
  end

  def self.decrypt(encrypted_data, secret, decrypted_file)
    data = RubyRNCryptor.decrypt(encrypted_data, secret)
    decrypted_file.write(data)
  rescue RuntimeError => e
    raise DecryptionError, e.message
  end

end
