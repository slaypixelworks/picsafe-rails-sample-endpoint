# encoding: utf-8

class EncryptedReportUploader < CarrierWave::Uploader::Base

  storage :file

  def store_dir
    Rails.root.join("tmp/reports")
  end

end
