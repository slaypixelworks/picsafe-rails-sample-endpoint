class ReportsController < ApplicationController

  protect_from_forgery except: :create

  # shows all reports that we have
  def index
    @reports = Report.order("created_at DESC").all
  end

  # returns decrypted report
  def show
    respond_to do |format|
      format.zip do
        report = Report.find(params[:id])
        file   = DecryptReport.perform(report)
        send_file file, type: 'application/zip', filename: "#{report.uuid}.zip"
      end
    end
  end

  # stores the report coming from the app
  def create
    StoreReport.perform(report_params)
    render json: { success: true }
  rescue StoreReport::Error => e
    render json: { success: false, error: e.message }
  end

  # deletes the report
  def destroy
    Report.find(params[:id]).destroy
    redirect_to :reports, notice: "Report has been deleted"
  end

  private

  def report_params
    params.permit(:token, :picsafe_file)
  end

end
