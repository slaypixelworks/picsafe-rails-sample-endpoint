# Rails Sample Code for a PicSafe Endpoint Server

This contains sample code for setting up an "Endpoint Server" for PicSafe (https://www.picsafe.com). It accepts reports posted from the PicSafe app and decrypts them. It is the code based used for the sample endpoint hosted at http://endpoint.picsafe.com and can be used as the basis for an institution to develop their own endpoint.

Although written in Rails, the code should reasonably easy to follow even if you are not familiar with Rails.

For detailed instructions please see: https://www.picsafe.com/integrations


## Deployment for sample endpoint

* Get the sources of the app from the repo.
* Run `gem install bundler` to get bundler
* Run `bundle install` to get all dependencies
* Run `ssh-add`
* Run `cap production deploy`


## Run locally

$ rails server -p 3000