class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :uuid, null: false
      t.string :encrypted_file, null: false
      t.string :keyserver_url, null: false

      t.string :owner_id, null: false
      t.string :owner_email
      t.string :owner_first_name
      t.string :owner_last_name
      t.string :owner_country

      t.timestamps null: false
    end

    add_index :reports, :uuid, unique: true
  end
end
