require 'test_helper'

class StoreReportTest < ActiveSupport::TestCase

  test "successful storing of the report" do
    assert_difference 'Report.count' do
      StoreReport.perform token: user_token, picsafe_file: picsafe_file
    end

    r = Report.last
    assert_equal "http://key.picsafe.com:81/api", r.keyserver_url
    assert_equal "0000000011462800528240", r.uuid
    assert_equal "679143898", r.owner_id
    assert_equal "jack@morgan.com", r.owner_email
    assert_equal "Jack", r.owner_first_name
    assert_equal "Morgan", r.owner_last_name
    assert_equal "AU", r.owner_country
    assert_equal 742610, r.encrypted_file.size
  end

  test "invalid token" do
    ex = assert_raise StoreReport::Error do
      StoreReport.perform token: "invalid", picsafe_file: picsafe_file
    end

    assert_equal "Invalid token", ex.message
  end

  test "no package json" do
    ex = assert_raise StoreReport::Error do
      StoreReport.perform token: user_token, picsafe_file: no_package_json_file
    end

    assert_equal "Invalid picsafe file (no package.json)", ex.message
  end

  test "no encrypted package" do
    ex = assert_raise StoreReport::Error do
      StoreReport.perform token: user_token, picsafe_file: no_encrypted_package_file
    end

    assert_equal "Invalid picsafe file (no package.encrypted)", ex.message
  end

  private

  def user_token
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Njc5MTQzODk4LCJlbWFpbCI6ImphY2tAbW9yZ2FuLmNvbSIsImZpcnN0X25hbWUiOiJKYWNrIiwibGFzdF9uYW1lIjoiTW9yZ2FuIiwidGl0bGUiOiJNciIsImNvdW50cnkiOiJBVSJ9.ebCtoAO88KNVoMkGGoRb96M6WhkevlOa3tGlc0GHGvc'
  end

  def picsafe_file
    fixture_file_upload '0000000011462800528240.picsafe'
  end

  def no_package_json_file
    fixture_file_upload 'no-package-json.picsafe'
  end

  def no_encrypted_package_file
    fixture_file_upload 'no-encrypted-package.picsafe'
  end

  def fixture_file_upload(path, mime_type = nil, binary = false)
    if self.class.respond_to?(:fixture_path) && self.class.fixture_path
      path = File.join(self.class.fixture_path, path)
    end
    Rack::Test::UploadedFile.new(path, mime_type, binary)
  end

end
