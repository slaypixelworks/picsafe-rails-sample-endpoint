require 'test_helper'
require 'zip'

class DecryptReportTest < ActiveSupport::TestCase

  test "successful decryption" do
    report, req = setup_report_and_request
    req.to_return(status: 200, body: { success: true, decryption_key: "1234567890" }.to_json)

    file = DecryptReport.perform(report)

    Zip::File.open(file) do |zip|
      assert_not_nil zip.glob("metadata.json")
      assert_not_nil zip.glob("report.pdf")
    end
  end

  test "invalid secret" do
    report, req = setup_report_and_request
    req.to_return(status: 200, body: { success: true, decryption_key: "invalid" }.to_json)

    ex = assert_raise DecryptReport::Error do
      DecryptReport.perform(report)
    end

    assert_equal "Unable to decrypt the report", ex.message
  end

  test "keyserver doesn't respond" do
    report, req = setup_report_and_request
    req.to_timeout

    ex = assert_raise DecryptReport::Error do
      DecryptReport.perform(report)
    end

    assert_equal "Unable to reach key server", ex.message
  end

  test "keyserver responds with an error" do
    report, req = setup_report_and_request
    req.to_return(status: 200, body: { success: false, error: "Some error" }.to_json)

    ex = assert_raise DecryptReport::Error do
      DecryptReport.perform(report)
    end

    assert_equal "Unable to get decryption key: Some error", ex.message
  end

  private

  def setup_report_and_request
    report = reports(:sample)
    req    = key_request_stub(report)
    return report, req
  end

  def key_request_stub(report)
    stub_request(:get, "#{report.keyserver_url}/decryption_key").
      with(query: { report_id: report.uuid, token: key_server_token })
  end

  def key_server_token
    Rails.application.secrets.key_server_token
  end

end
