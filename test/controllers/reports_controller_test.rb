require "test_helper"

class ReportsControllerTest < ActionController::TestCase

  test "store report from a known user" do
    StoreReport.expects(:perform)
    post :create, token: user_token, picsafe_file: picsafe_file
    assert_response_json(success: true)
  end

  test "failed storing of the report" do
    StoreReport.expects(:perform).raises(StoreReport::Error, "Invalid token")
    post :create
    assert_response_json(success: false, error: "Invalid token")
  end

  test "decrypt the report successfully" do
    DecryptReport.expects(:perform).returns(fixture_file_upload('0000000011462800528240.picsafe'))
    report = reports(:sample)
    get :show, id: report.id, format: 'zip'
  end

  private

  def user_token
    'some-user-token'
  end

  def picsafe_file
    fixture_file_upload '0000000011462800528240.picsafe'
  end

end
