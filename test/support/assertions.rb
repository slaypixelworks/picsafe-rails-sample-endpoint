def response_json
  ActiveSupport::HashWithIndifferentAccess.new(JSON.parse(response.body))
end

# asserts response json matches the expected
def assert_response_json(json)
  assert_equal ActiveSupport::HashWithIndifferentAccess.new(json), response_json
end
